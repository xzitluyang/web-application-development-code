# 一、 Vuex 介绍与基础使用示例
Vuex 是 Vue.js 的官方状态管理模式和库，它采用集中式存储管理应用的所有组件的状态，并在组件间进行高效的状态更新和共享。以下是 Vuex 的功能介绍以及基础使用示例。
## Vuex 简介
在 Vue 中，数据驱动和组件化是核心概念。每个组件拥有自己的 `data`、`template` 和 `methods`。在单组件中修改状态更新视图是方便的，但当多个组件（甚至是多层嵌套的组件）需要共享同一个状态时，使用 Vuex 进行状态管理就显得尤为重要。

## Vuex 解决的问题
- **多个视图依赖同一个状态**：Vuex 允许你将这些共享状态集中存储在一个地方。
- **不同视图的行为需要变更同一个状态**：通过 Vuex，你可以集中处理逻辑，易于维护。

## 使用 Vuex 的好处
- **集中管理共享数据**：易于开发和后期维护。
- **高效实现组件间数据共享**：提高开发效率。
- **响应式数据**：Vuex 中的数据都是响应式的。

![vuex原理图](vuex原理图.png)

# 二、 动手实践

## 1. 首先创建一个空的vue项目
vue create <项目名称> 

（在初始化项目时可以直接选择安装vuex）


## 2. Vuex 基础使用
### 2.1 安装 Vuex
通过`npm` 安装 Vuex：
```bash
npm install vuex --save
```

### 2.2 创建 Store
在项目的 `src` 目录下创建 `store` 目录，并添加 `index.js` 文件：

如果是在创建项目时已经安装过，可以跳过这一小节

需要共享的状态都放在写在 state 对象里面 

```javascript
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    name: '张三',
    age: 21,
  },
  mutations: {},
  actions: {},
  modules: {},
});
```

### 2.3 挂载 Store
在 `main.js` 中将 Store 挂载到 Vue 实例上：
```javascript
import Vue from 'vue';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
```

## 3. Vuex的基本使用

首先在Vue中添加 Vuex 插件
```npm install vuex --save```
通过 npm 进行安装 Vuex 后，在项目的 src 目录下会多出一个 store 目录，目录下会有个 index.js

```js
/* src/store/index.js */

// 导入 Vue
import Vue from 'vue'
// 导入 Vuex 插件
import Vuex from 'vuex'

// 把 Vuex 注册到Vue 上
Vue.use(Vuex)

export default new Vuex.Store({
  // 在开发环境开启严格模式 这样修改数据 就必须通过 mutation 来处理
  strict:products.env.NODE_ENV !== 'production',
  // 状态
  state: {
  },
  // 用来处理状态
  mutations: {
  },
  // 用于异步处理
  actions: {
  },
  // 用来挂载模块
  modules: {
  }
})
```
要使用 store 就在把 store 挂载到 Vue 中

把 store 挂载到 Vue 之后 ，所有的组件就可以直接从 store 中获取全局数据了

```js
import Vue from 'vue'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false

new Vue({
  // 挂载到vue 中
  store,
  render: (h) => h(App),
}).$mount('#app')
```
### 1.state

在 state 中添加数据,我们需要共享的状态都放在写在 state 对象里面

```js
/* src/store/index.js */

// 导入 Vue
import Vue from 'vue'
// 导入 Vuex 插件
import Vuex from 'vuex'

// 把 Vuex 注册到Vue 上
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '张三',
    age: 21,
  },
  mutations: {},
  actions: {},
  modules: {},
})
```
组件中获取 state 中的数据

获取到 state 有两种方式

1.1 直接使用 this.$store.state[属性] ，（this 可以省略）
```js
<template>
  <div id="app">
    {{ this.$store.state.name }}
    {{ this.$store.state.age }}
  </div>
</template>
```
1.2 使用 mapState
通过 mapState把 store 映射到 组件的计算属性，就相当于组件内部有了 state 里的属性

```js
<template>
  <div id="app">
    {{ name }}
    {{ age }}
  </div>
</template>

<script>
// 从 Vuex 中导入 mapState
import { mapState } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 store 映射到当前组件的计算属性
    ...mapState(['name', 'age'])
  }
}
</script>

<style  scoped>
</style>
```
`注意:
当store 中的值 和 当前组件有相同的状态，我们可以在 mapState 方法里传递一个对象 而不是一个数组，在对象中给状态起别名`

```js
computed: {
    // name2 和 age2 都是别名
    ...mapState({ name2: 'name', age2: 'age'}])
}
```
### 2.Mutation
Store 中的状态不能直接对其进行操作，我们得使用 Mutation 来对 Store 中的状态进行修改，虽然看起来有些繁琐，但是方便集中监控数据的变化。

state 的更新必须是 Mutation 来处理

首先在 mutaions 里定义个方法

如果想要定义的方法能够修改 Store 中的状态，需要参数就是 state

```js
/* src/store/index.js */

// 导入 Vue
import Vue from 'vue'
// 导入 Vuex 插件
import Vuex from 'vuex'

// 把 Vuex 注册到Vue 上
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '张三',
    age: 21,
  },
  mutations: {
    // 在这里定义 方法
    /**
     *
     * @param {*} state 第一个参数是 Store 中的状态(必须传递)
     * @param {*} newName 传入的参数 后面是多个
     */
    changeName(state, newName) {
      // 这里简单举个例子 修改个名字
      state.name = newName
    },
  },
  actions: {},
  modules: {},
})
```
在组件中使用 mutations 中的方法

同样有两种方法在组件触发 mutations 中的方法

2.1 this.$store.commit() 触发

在 methods 中定义一个方法，在这个方法里面进行触发 mutations 中的方法

```js
<template>
  <div id="app">
    <button @click="handleClick">方式1 按钮使用 mutation 中方法</button>
    {{ name }}
  </div>
</template>

<script>

// 从 Vuex 中导入 mapState
import { mapState } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 store 映射到当前组件的计算属性
    ...mapState(['name', 'age'])
  },
  methods: {
    handleClick() {
      // 触发 mutations 中的 changeName
      this.$store.commit('changeName', '小浪')
    }
  },
}
</script>

<style  scoped>
</style>
```

2.2 使用 mapMutations
```js
<template>
  <div id="app">
    <button @click="changeName('小浪')">方式2 按钮使用 mutation 中方法</button>
    {{ name }}
  </div>
</template>

<script>

// 从 Vuex 中导入 mapState
import { mapState, mapMutations } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 store 映射到当前组件的计算属性
    ...mapState(['name', 'age'])
  },
  methods: {
	// 将 mutations 中的 changeName 方法映射到 methods 中，就能直接使用了 changeName 了
    ...mapMutations(['changeName'])
  },
}
</script>

<style  scoped>
</style>
```

### 3.Action
Action 和 Mutation 区别

Action 同样也是用来处理任务，不过它处理的是异步任务，异步任务必须要使用 Action，通过 Action 触发 Mutation 间接改变状态，不能直接使用 Mutation 直接对异步任务进行修改

先在 Action 中定义一个异步方法来调用 Mutation 中的方法

```js
/* src/store/index.js */

// 导入 Vue
import Vue from 'vue'
// 导入 Vuex 插件
import Vuex from 'vuex'

// 把 Vuex 注册到Vue 上
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '张三',
    age: 21,
  },
  mutations: {
    // 在这里定义 方法
    /**
     *
     * @param {*} state 第一个参数是 Store 中的状态(必须传递)
     * @param {*} newName 传入的参数 后面是多个
     */
    changeName(state, newName) {
      // 这里简单举个例子 修改个名字
      state.name = newName
    },
  },
  actions: {
    /**
     *
     * @param {*} context 上下文默认传递的参数
     * @param {*} newName 自己传递的参数
     */
    // 定义一个异步的方法 context是 store
    changeNameAsync(context, newName) {
      // 这里用 setTimeout 模拟异步
      setTimeout(() => {
        // 在这里调用 mutations 中的处理方法
        context.commit('changeName', newName)
      }, 2000)
    },
  },
  modules: {},
})
```
在组件中是 Action 中的异步方法也是有两种方式

3.1 this.$store.dispatch()
```js
<template>
  <div id="app">
    <button @click="changeName2('小浪')">方式1 按钮使用 action 中方法</button>
    {{ name }}
  </div>
</template>

<script>

// 从 Vuex 中导入 mapState mapMutations
import { mapState, mapMutations } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 store 映射到当前组件的计算属性
    ...mapState(['name', 'age'])
  },
  methods: {
    changeName2(newName) {
      // 使用 dispatch 来调用 actions 中的方法
      this.$store.dispatch('changeNameAsync', newName)
    }
  },
}
</script>

<style  scoped>
</style>
```

2.使用 mapActions
```js
<template>
  <div id="app">
    <button @click="changeNameAsync('小浪')">
      方式2 按钮使用 action 中方法
    </button>
    {{ name }}
  </div>
</template>

<script>

// 从 Vuex 中导入 mapState mapMutations mapActions
import { mapState, mapMutations, mapActions } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 store 映射到当前组件的计算属性
    ...mapState(['name', 'age'])
  },
  methods: {
    // 映射 actions 中的指定方法 到 methods中，就可以在该组件直接使用
    ...mapActions(['changeNameAsync'])
  },
}
</script>

<style  scoped>
</style>
```

### 4.Getter

Getter 类似于计算属性，但是我们的数据来源是 Vuex 中的 state ,所以就使用 Vuex 中的 Getter 来完成

应用场景：

需要对 state 做一些包装简单性处理 展示到视图当中，先来写个 Getter

```js
/* src/store/index.js */

// 导入 Vue
import Vue from 'vue'
// 导入 Vuex 插件
import Vuex from 'vuex'

// 把 Vuex 注册到Vue 上
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '张三',
    age: 21,
  },
  getters: {
    // 在这里对 状态 进行包装
    /**
     *
     * @param {*} state 状态 如果要使用 state 里面的数据，第一个参数默认就是 state ，名字随便取
     * @returns
     */
    decorationName(state) {
      return `大家好我的名字叫${state.name}今年${state.age}岁`
    },
  },
})
```

Getter 也有两种方式导入

4.1 this.$store.getters[名称]
```js
<template>
  <div id="app">
    {{ this.$store.getters.decorationName }}
  </div>
</template>
```
4.2 使用 mapGetters
```js
<template>
  <div id="app">
    {{ decorationName }}
  </div>
</template>

<script>

// 从 Vuex 中导入 mapGetters
import { mapGetters } from 'vuex'
export default {
  name: 'App',
  computed: {
    // 将 getter 映射到当前组件的计算属性
    ...mapGetters(['decorationName'])
  },
}
</script>
```
### 5. Module
由于使用单一状态树，应用的所有状态会集中到一个比较大的对象。当应用变得非常复杂时，store对象就有可能变得相当臃肿。为了使 store 更易于管理，Vuex 允许将 store 分割成模块。每个模块拥有自己的 `state`、`getters`、`actions`、`mutations`。
```js

const moduleA = {
  state: { ... },
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}

const moduleB = {
  state: { ... },
  mutations: { ... },
  actions: { ... }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})

store.state.a // -> moduleA 的状态
store.state.b // -> moduleB 的状态
```

# 三、跟着例子练一练
https://juejin.cn/post/7280007176776204327