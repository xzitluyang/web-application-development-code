import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    name: '张三',
    age: 21
  },
  getters: {
    getName(state) {
      return `我的名字是${state.name}`
    }
  },
  mutations: {
    changeName(state, newName) {
      state.name = newName
    }
  },
  actions: {
    changeNameAsync(context,newName) {      
      setTimeout(() => {
        context.commit('changeName', newName)
      }, 3000)
    }
  },
  modules: {
  }
})
