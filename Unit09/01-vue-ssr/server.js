// 第1步：创建一个Vue实例
const Vue = require('vue')
const app = new Vue({
  template: `<div>Good luck</div>`
})
// 第2步：创建一个renderer
const renderer = require('vue-server-renderer').createRenderer()
// 第3步：将Vue实例渲染为 HTML
renderer.renderToString(app, (err, html) => {
  if (err) throw err
  console.log(html)
  // => <div data-server-rendered="true">Good luck</div>
})

