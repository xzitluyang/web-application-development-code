// ① 创建Vue实例
const Vue = require('vue')
const server = require('express')()
// ② 读取模板
const renderer = require('vue-server-renderer').createRenderer({
  template: require('fs').readFileSync('./template.html', 'utf-8')
})
const context = {
    title: 'vue ssr',
    metas: `
        <meta name="keyword" content="vue,ssr">
        <meta name="description" content="vue srr demo">
    `,
}
// ③ 处理GET方式请求
server.get('*', (req, res) => {
  res.set({'Content-Type': 'text/html; charset=utf-8'})
  const vm = new Vue({
    data: {
      url: req.url
    },
	//使用双花括号进行HTML转义插值
    template: '<div>当前访问的URL是：{{ url }}</div>',
  })
  // ④ 将Vue实例渲染为HTML后输出
  renderer.renderToString(vm,context, (err, html) => {
    if (err) {
      res.status(500).end('err: ' + err)
      return
    }
    res.end(html)
  })
})
server.listen(8080, function () {
  console.log('server started at localhost:8080')
})
