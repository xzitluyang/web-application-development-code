// renderer01.js代码如下：
const Vue = require('vue')
// 创建渲染器
const renderer = require('vue-server-renderer').createRenderer()
const app = new Vue({
  template: `<div>Good luck</div>`
});
// 生成预渲染的HTML字符串，如果没有传入回调函数，则会返回promise 
renderer.renderToString(app).then(html => {
  console.log(html)  // 输出：<div data-server-rendered="true">Good luck</div>
}).catch(err => {
  console.log(err)
});
// 当然我们也可以使用另外一种方式渲染，传入回调函数，
// 其实和上面的结果一样，只是两种不同的方式而已
renderer.renderToString(app, (err, html) => {
  if (err) {
    throw err
    return
  }
  console.log(html)
  // => <div data-server-rendered="true">Good luck</div>
})
