/*
Navicat MySQL Data Transfer
Source Server         : 127.0.0.1_3306
Source Server Version : 60011
Source Host           : 127.0.0.1:3306
Source Database       : test
Target Server Type    : MYSQL
Target Server Version : 60011
File Encoding         : 65001
Date: 2022-03-19 14:13:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `prods_list`
-- ----------------------------
DROP TABLE IF EXISTS `prods_list`;
CREATE TABLE `prods_list` (
  `id` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `add_time` datetime DEFAULT NULL,
  `abstract` varchar(255) DEFAULT NULL,
  `click` tinyint(4) DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `market_price` double DEFAULT NULL,
  `stock_quantity` tinyint(4) DEFAULT NULL,
  `goods_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of prods_list
-- ----------------------------
INSERT INTO `prods_list` VALUES ('1', 'HTML5+CSS3移动Web开发实战', '2022-02-26 15:07:22', '从跨平台移动Web开发实际应用的角度阐述了HTML5和CSS3的新元素和新功能。', '1', 'img/01.jpg', '45.20', '58.00', '10', '12602402');
INSERT INTO `prods_list` VALUES ('2', '给Python点颜色 青少年学编程', '2022-03-13 15:07:22', '用编程解决数学方面的问题，并绘制一个美丽的图案', '1', 'img/02.jpg', '33.80', '59.80', '8', '12563157');
INSERT INTO `prods_list` VALUES ('3', '零基础学Python（全彩版）', '2022-02-26 15:07:22', '详细地介绍了使用 IDLE 及 Python 框架进行程序管理的知识和技术。', '1', 'img/03.jpg', '73.40', '79.80', '5', '12353915');
INSERT INTO `prods_list` VALUES ('4', '数学之美（第二版）', '2022-03-17 10:48:36', '《数学之美》上市后深受广大读者欢迎，并荣获国家图书馆第八届文津图书奖', '1', 'img/04.jpg', '24.50', '49.00', '20', '11572052');
INSERT INTO `prods_list` VALUES ('5', 'Python编程 从入门到实践', '2022-06-12 10:18:26', '本书是一本针对所有层次的Python读者而作的Python入门书。', '1', 'img/05.jpg', '70.20', '89.00', '15', '11993134');
