# store-server
## 接口文档
[接口文档](https://github.com/hai-27/store-server/blob/master/docs/API.md)
## 运行项目
```
1. Clone project

git clone https://github.com/hai-27/store-server.git

2. Project setup

cd store-server
npm install

3. Run project

node app.js
```
