module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential"
    ],
    "parserOptions": {
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
    "indent": [
        "error",
        "tab"
    ],
    "linebreak-style": [
        "error",
        "windows"
    ],
    "quotes": [
        "error",
       // 改成字符串必须由单引号括起来而不是双引号，'string'不报错，"string"报错
        "single" 
    ],
    "semi": [
        "error",
         // 改成代码结尾不再加分号，加了分号报错，不加分号不报错
        "never" 
    ],
    // 0 相当于off，表示关闭规则，相当于不再校验这条规则：变量定义了必须使用
    "no-unused-vars": 0 
 }

};

