const express = require('express')
const app = express()
app.get('/', function (req, res) {
  res.send('Hello Express!')
})
var server = app.listen(8081, function () { 
  var host = server.address().address
  var port = server.address().port 
  console.log("本应用程序的访问地址为 http://%s:%s", host, port)
})