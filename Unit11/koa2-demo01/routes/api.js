const router = require("koa-router")();
// 定义路由前缀（当前路由的路径）
router.prefix("/api");
// 处理二级路由下的 login 路径
router.get('/login', async (ctx, next) => {
    // ctx.body 表示向客户端发送的数据
    ctx.body = {
        ctx
    }
})
// 导出配置完的 router
module.exports = router

