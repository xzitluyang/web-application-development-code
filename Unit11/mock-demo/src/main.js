import Vue from "vue";

import App from "./App.vue";
import router from "./router";
import './mock'

// 环境配置，是否为生成环境
// false为开发环境，Vue会提供很多警告，方便调试代码
// true为生成环境，Vue会提供很少的警告
Vue.config.productionTip = false;

console.log(process.env.VUE_APP_BASE_API)

new Vue({
  router,
  render: (h) => h(App)  
}).$mount("#app");
