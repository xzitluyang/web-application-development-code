module.exports = {
    devServer: {
        port: 8081,         // 端口号，如果端口被占用，会自动加1
        host: "localhost",  // 主机名，127.0.0.1  真机 0.0.0.0
        https: false,       // 协议
        open: true,         // 启动服务时自动打开浏览器访问
	 // 项目根目录下的 vue.config.js 的 devServe 中
      before: () => {
          var chokidar = require('chokidar')  // 监听
          var watcher = chokidar.watch('./src/mock/', {
              ignored: /(^|[\/\\])\../,
              persistent: true
          })
          var forkMockServer = function () {
              return require('child_process').fork('./src/mock/index.js')  // 子进程,fork父子通信
          }
          var n = forkMockServer()

          watcher.on('change', () => {
              console.log('restart mock server')
              n.kill()
              n = forkMockServer()
          })

          process.on('SIGINT', function () {
              n.kill()
              process.exit(0)
          })
      }

    },
    lintOnSave: false,       // 关闭格式检查
    productionSourceMap: false // 打包时不会生成.map文件，加快打包速度
}