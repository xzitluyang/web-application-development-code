var http = require('http')
http.createServer(function (request, response) {
    // 发送HTTP头部 
    // HTTP状态值: 200
    // 内容类型:text/plain
    response.writeHead(200, {'Content-Type': 'text/plain'})
    // 发送响应数据 "Hello Node.js"
    response.end('Hello Node.js\n')
}).listen(8081)
// 终端输出"Hello Node.js"
console.log('Server running at http://127.0.0.1:8081/')