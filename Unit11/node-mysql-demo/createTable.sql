
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT '',
  `mobile` varchar(16) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', '123456', 'abcefg@163.com', '13307341234'), ('2', 'better', '6688', '123456@qq.com', '18007316688');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
